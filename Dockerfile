FROM nginx:latest

ENV APP="/app/spa"
ARG APP=$APP

RUN mkdir -p /usr/share/nginx/html/$APP

COPY ./app /usr/share/nginx/html/
COPY ./app /usr/share/nginx/html/$APP
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

